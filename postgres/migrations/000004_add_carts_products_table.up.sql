BEGIN;

CREATE TABLE public.carts_products (
    id bigint NOT NULL,
    created_at timestamp with time zone,
    updated_at timestamp with time zone,
    deleted_at timestamp with time zone,
    cart_id bigint NOT NULL,
    product_id bigint NOT NULL,
    amount bigint NOT NULL
);

ALTER TABLE public.carts_products OWNER TO krup;

CREATE SEQUENCE public.carts_products_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.carts_products_id_seq OWNER TO krup;

ALTER SEQUENCE public.carts_products_id_seq OWNED BY public.carts_products.id;

ALTER TABLE ONLY public.carts_products ALTER COLUMN id SET DEFAULT nextval('public.carts_products_id_seq'::regclass);

ALTER TABLE ONLY public.carts_products
    ADD CONSTRAINT carts_products_pkey PRIMARY KEY (id);

CREATE INDEX idx_carts_products_deleted_at ON public.carts_products USING btree (deleted_at);

ALTER TABLE ONLY public.carts_products
    ADD CONSTRAINT fk_carts_carts_products FOREIGN KEY (cart_id) REFERENCES public.carts(id) ON DELETE CASCADE;

ALTER TABLE ONLY public.carts_products
    ADD CONSTRAINT fk_products_carts_products FOREIGN KEY (product_id) REFERENCES public.products(id) ON DELETE CASCADE;

COMMIT;
