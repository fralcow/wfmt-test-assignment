package main

import (
	"errors"
	"net/http"

	log "github.com/sirupsen/logrus"
	"golang.org/x/crypto/bcrypt"
	"gorm.io/gorm"

	"github.com/gin-gonic/gin"
)

func main() {
	err := setEnvVars()
	if err != nil {
		log.Fatal(err)
	}

	setupLogs()

	r := setupRouter()

	r.Run(":3000")
}

func setupRouter() *gin.Engine {
	r := gin.New()

	r.Use(gin.Logger())
	r.Use(gin.Recovery())

	v1 := r.Group("/v1")
	v1.POST("/token", postTokenHandler)

	authorized := v1.Group("/")
	authorized.Use(AuthRequired())
	{
		authorized.GET("/products", getProductsHander) //TODO manager and user
		product := authorized.Group("/product")
		{
			product.POST("/", postProductHandler)        //TODO manager
			product.GET("/:id", getProductHandler)       //TODO manager and user
			product.PATCH("/:id", patchProductHandler)   //TODO manager
			product.DELETE("/:id", deleteProductHandler) //TODO manager
		}

		authorized.GET("/carts", getCartsHandler) //TODO manager
		cart := authorized.Group("/cart")
		{
			cart.GET("/cart/:id", getCartHandler)                      //TODO manager and user (user can only get their own cart)
			cart.POST("/cart/:id/product", postCartProductHandler)     //TODO user (can only post to their own cart)
			cart.PATCH("/cart/:id/product", patchCartProductHandler)   //TODO user (can only post to their own cart)
			cart.DELETE("/cart/:id/product", deleteCartProductHandler) //TODO user (can only post to their own cart)
		}

	}
	return r
}

func AuthRequired() gin.HandlerFunc {
	return func(c *gin.Context) {
		t, ok := c.Request.Header["Authorization"]
		if ok != true {
			err := errors.New("Missing authorization header")
			log.Error(err)
			c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{"Error": err.Error()})
			return
		}

		if len(t) != 1 {
			err := errors.New("Error decoding token")
			log.Error(err)
			c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{"Error": err.Error()})
			return
		}

		log.Debug(t)

		_, err := validateToken(t[0])
		if err != nil {
			log.Error(err)
			c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{"Error": "Unathorized"})
			return
		}

	}
}

type postToken struct {
	Account  string `json:"account"`
	Password string `json:"password"`
}

func postTokenHandler(c *gin.Context) {
	pt := postToken{}

	err := c.BindJSON(&pt)
	if err != nil {
		log.Error(err)
		c.AbortWithStatus(http.StatusBadRequest)
		return
	}

	log.Debug(pt)

	u, err := authenticateUser(pt.Account, pt.Password)
	if err != nil {
		if errors.Is(err, bcrypt.ErrMismatchedHashAndPassword) ||
			errors.Is(err, gorm.ErrRecordNotFound) {
			c.JSON(http.StatusUnauthorized, gin.H{"Error": "Wrong account or password"})
		}

		log.Error(err)
		c.AbortWithStatus(http.StatusBadRequest)
		return
	}

	ts, err := GenerateBearer(u.Account, u.Type)
	if err != nil {
		log.Error(err)
		c.AbortWithStatus(http.StatusBadRequest)
	}

	c.JSON(http.StatusOK, gin.H{"Bearer": ts})
}

func getProductsHander(c *gin.Context) {
	c.JSON(http.StatusNotImplemented, gin.H{"Error": "Not implemented"})
}

func postProductHandler(c *gin.Context) {
	c.JSON(http.StatusNotImplemented, gin.H{"Error": "Not implemented"})
}

func getProductHandler(c *gin.Context) {
	c.JSON(http.StatusNotImplemented, gin.H{"Error": "Not implemented"})
}

func patchProductHandler(c *gin.Context) {
	c.JSON(http.StatusNotImplemented, gin.H{"Error": "Not implemented"})
}

func deleteProductHandler(c *gin.Context) {
	c.JSON(http.StatusNotImplemented, gin.H{"Error": "Not implemented"})
}

func getCartsHandler(c *gin.Context) {
	c.JSON(http.StatusNotImplemented, gin.H{"Error": "Not implemented"})
}

func getCartHandler(c *gin.Context) {
	c.JSON(http.StatusNotImplemented, gin.H{"Error": "Not implemented"})
}

func postCartProductHandler(c *gin.Context) {
	c.JSON(http.StatusNotImplemented, gin.H{"Error": "Not implemented"})
}

func patchCartProductHandler(c *gin.Context) {
	c.JSON(http.StatusNotImplemented, gin.H{"Error": "Not implemented"})
}

func deleteCartProductHandler(c *gin.Context) {
	c.JSON(http.StatusNotImplemented, gin.H{"Error": "Not implemented"})
}
